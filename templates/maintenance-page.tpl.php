 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
  <?php include_once('includes/uk-global-header.inc'); ?>
</head>

<body class="<?php print $classes; ?>">
	<div id="page-wrapper"  class="middle-container">
		<div id="page">
	
			<!-- start header -->
			<?php include('includes/header.inc'); ?>
			<?php // print render($page['header']); ?>
			<!-- end header --> 
   
			<div id="main" class="wrap-inner cf row">
				<section id="content" class="col-md-12">
					<center>
					<h1>We are currently undergoing maintenance.  We should be back shortly. Thank you for your patience.</h1>
					<br>
					<br>
				</section>
			</div>
		</div>
	
		<!-- end content -->

        <?php include('includes/uk-global-footer.inc'); ?>

	</div>
</body>

