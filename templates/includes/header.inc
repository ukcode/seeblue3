<?php
$default_banner = theme_get_setting('default_banner');

$description = theme_get_setting("site_description");
//$fid = theme_get_setting('lockup');
//$file = file_load($fid);
//$o = file_create_url($file->uri);
// If the filename contains the word tile, tile the background instead of centering it. 
$bgfile = theme_get_setting('background_logo_path');

if(strpos($bgfile,'-tile') !== false) {
	$whichheader = "";
	$background = "#2C2A29 url(" . file_create_url($bgfile) . ") center center;";
	$bgsize = "auto;";
} else {
	$whichheader = "-inner .wrap-inner ";
	$background = "#2C2A29 url(" . file_create_url($bgfile) . ") left center no-repeat;";
	$bgsize = "cover;";
}

$fp = variable_get('site_frontpage', 'node');

$apath = base_path( ) . drupal_get_path('theme', variable_get('theme_default', NULL));

$banner_fid = theme_get_setting('homepage_banner');
if ($banner_fid){
	$banner_file = file_load($banner_fid);
	$banner_url = file_create_url($banner_file->uri);
} else {
//    print theme_get_setting('default_lockup');
//    $banner_url = $apath . '/img/styleguide/uk-lockup_white.svg';
}


?>
<style type="text/css">
/*	#header {*/
/*		background: #2C2A29;*/
/*		/* border-bottom: 10px solid #0033a0; */*/
/*	}*/
/*	#header*/<?php //print $whichheader; ?>/* {*/
/*		background: */<?php //print $background; ?>/*;*/
/*		background-size: */<?php //print $bgsize; ?>/*;*/
/*	}*/
</style>
<header id="header" style="position:relative">
	<div id="header-inner" class="container uky-offcanvas__content"">
		<?php if ($subsite_title = theme_get_setting('subsite_title')):?>
			<div class="subsite-title row">
				<h1 class="col-md-10 col-md-offset-1"><?php echo $subsite_title; ?></h1>
			</div>
		<?php endif; ?>
		<div class="wrap-inner section cf row">
			<?php if ($description){?>
			<div class="site-description col-md-8 col-md-offset-2">
				<p><?php print $description; ?></p>
			</div>
			<?php } ?>
		</div>
	</div>
	<div class="header-banner" style="background-image: url('<?php //if (isset($banner_url)) {print $banner_url;}?><?php if (isset($banner_url)) {print $banner_url;}else{print $default_banner;}?>');
	"></div>
</header>