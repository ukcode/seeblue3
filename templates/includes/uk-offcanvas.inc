<div class="uky-offcanvas" aria-hidden="true">

    <div class="uky-offcanvas__search">
        <form method="POST" action="<?= $GLOBALS['base_url']; ?>/search">
            <svg>
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#search"></use>
            </svg>
            <input class="uky-offcanvas__search__input" type="search" placeholder="Search..." name="keys" />
			<!-- <input  class="uk-search-bar__submit" type="submit" name="submit" value="Search"> -->
        </form>
    </div>

    <div class="uky-offcanvas__list uky-verticalmenu">
	<a href="<?php echo $front_page; ?>" class="title"><div class="home-offcanvas"><i class="fa fa-home"></i></div></a>
        <?php $tree_output =  menu_tree_output(menu_tree_all_data($menu_selection, 0)); ?>
        <?php print drupal_render($tree_output); ?>
    </div>
</div>