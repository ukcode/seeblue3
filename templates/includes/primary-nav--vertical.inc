<?php if ($is_horizontal == 0 && $is_front == TRUE){ ?>

    <nav class="primary-nav--vertical hidden-sm hidden-xs uky-verticalmenu">
	    <?php $tree_output =  menu_tree_output(menu_tree_all_data($menu_selection, 0)); ?>
		<?php print drupal_render($tree_output); ?>
    </nav>

<?php } elseif ($is_horizontal == 0) {?>

	<nav class="primary-nav--vertical hidden-sm hidden-xs uky-verticalmenu">
	<a href="<?php echo $front_page; ?>" class="title"><div class="home-sidebar"><i class="fa fa-home"></i></div></a>
        <?php $tree_output =  menu_tree_output(menu_tree_all_data($menu_selection, 0)); ?>
		<?php print drupal_render($tree_output); ?>
    </nav>
	
<?php } ?>

