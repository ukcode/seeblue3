<?php if($variables['social_location'] != 4 && $variables['social_location'] == 1 || $variables['social_location'] == 3): ?>
    <?php if ($variables['socialMediaLinks']['sidebar']){?>

        <aside class="uk-socialmedia uk-socialmedia--sidebar
         <?php if (count($variables['socialMediaLinks']['sidebar']) > 3): print "uk-socialmedia--compact"; endif; ?>
        ">
            <ul class="uk-socialmedia__list">
                <?php foreach($variables['socialMediaLinks']['sidebar'] as $accountName => $account){ ?>
                    <li class="uk-socialmedia__list-item">
                        <a class="uk-socialmedia__link"href="<?php echo $account['url']; ?>" target="_blank"><span class="uk-socialmedia__name"><?php print $accountName;?></span><i class="fa <?php echo $account['icon']; ?>" ></i></a>
                    </li>
                <?php };?>
            </ul>
        </aside>
    <?php }; ?>
<?php endif; ?>
