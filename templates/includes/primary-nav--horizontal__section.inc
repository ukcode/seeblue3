<?php if ($is_horizontal == 1){
    $subtree = seeblueV3_menu_topSectionTree();
    if (isset($subtree)) {
        $keys = array_keys($subtree);

        if (count($subtree[$keys[0]]['below'])) {
			$subtree_output = menu_tree_output($subtree);
        }
    }

?>

<nav class="primary-nav--vertical hidden-sm hidden-xs uky-verticalmenu">
    <a href="<?php echo $front_page; ?>" class="title"><div class="home-sidebar"><i class="fa fa-home"></i></div></a>
	<?php print drupal_render($subtree_output);?>
</nav>

<?php } ?>
