<?php if ($is_horizontal == 1): ?>
    <nav class="primary-nav--horizontal  hidden-sm hidden-xs row">
        <div class="col-md-12">
			<?php $main_menu =  menu_tree_output(menu_tree_all_data($menu_selection, 0)); ?>
          	<?php print drupal_render($main_menu); ?>
        </div>
    </nav>
<?php endif; ?>