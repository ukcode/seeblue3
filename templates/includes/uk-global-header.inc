<?php
$apath = base_path( ) . drupal_get_path('theme', variable_get('theme_default', NULL));

$lockup_fid = theme_get_setting('lockup');
if ($lockup_fid){
    $lockup_file = file_load($lockup_fid);
    $lockup_url = file_create_url($lockup_file->uri);
} else {
    print theme_get_setting('default_lockup');
    $lockup_url = $apath . '/img/styleguide/uk-lockup_white.svg';
}
 ?>


<div class="uk-global-header">
    <div class="uk-search-bar" aria-hidden="true" id="uk-global-header-searchbar">

        <svg xmlns="http://www.w3.org/2000/svg" style="display:none;">
            <symbol id="x" Viewbox="-110.7 118.8 23.3 23.3">
                <path d="M-87.3 121l-2.2-2.2-9.5 9.6-9.5-9.6-2.2 2.2 9.6 9.5-9.6 9.5 2.2 2.2 9.5-9.6 9.5 9.6 2.2-2.2-9.6-9.5"></path>
            </symbol><!--end x-->
        </svg>

        <div class="uk-search-bar__container">
            <div class="uk-search-bar__row">

                <div class="uk-search-bar__menu">
                    <a class="uk-search-bar__menu-item" href="http://www.uky.edu/Directory/">People</a>
                    <a class="uk-search-bar__menu-item" href="http://maps.uky.edu/campusmap/">Map</a>
                </div>


                <div class="uk-search-bar__form">
					<form method="POST" action="<?= $GLOBALS['base_url']; ?>/search">
						<input class="uk-search-bar__input" type="search" name="keys" />
						<input class="uk-search-bar__submit" type="submit" name="submit" value="Search" />
					</form>

                    <button type="button" title="Close" class="uk-search-bar__dismiss">
                        <svg height="30px" width="30px"><use xlink:href="#x"></use></svg>
                        <span class="hide">Close</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <svg xmlns="http://www.w3.org/2000/svg" style="display:none;">
        <symbol id="search" viewBox="0 0 16.7 16.8">
            <path d="M9.7 0c-3.9 0-7 3.1-7 7 0 1.6.5 3.1 1.4 4.2L0 15.4l1.4 1.4 4.2-4.2c1.2.9 2.6 1.4 4.1 1.4 3.9 0 7-3.1 7-7s-3.1-7-7-7zm0 12c-2.8 0-5-2.2-5-5s2.2-5 5-5 5 2.2 5 5-2.2 5-5 5z" class="st0"></path>
        </symbol>
    </svg>

    <header>
        <div class="uk-global-header__logo">
            <img src="<?php print $lockup_url; ?>" />
        </div>

        <div class="uk-global-header__left-nav">

        </div>

        <div class="uk-global-header__right-nav">
            <ul class="uk-global-header__nav-list">
                <li class="uk-global-header__nav-item uk-global-header__myUK hidden-xs hidden-sm"><a href="http://myuk.uky.edu" target="_blank">myUK</a></li>
                <li class="uk-global-header__nav-item uk-global-header__search-button ">
                    <button aria-expanded="false" aria-controls="uk-global-header-searchbar">
                        <svg>
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#search"></use>
                        </svg>
                    </button>
                </li>
                <li class="uk-global-header__nav-item visible-sm-inline visible-xs-inline">
                    <button class="uky-offcanvas__button" title="Section navigation menu" aria-label="Main navigation menu" role="button" aria-controls="offcanvas-navigation" aria-expanded="false">
                        <span class="uky-offcanvas__button-text">Menu</span>
                        <i class="fa fa-bars"></i>
                    </button>
                </li>
            </ul>
        </div>
    </header>
</div>