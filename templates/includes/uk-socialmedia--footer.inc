<?php if($variables['social_location'] != 4 && $variables['social_location'] == 2 || $variables['social_location'] == 3): ?>
	<?php if ($variables['socialMediaLinks']['footer']){?>

		<div class="uk-socialmedia uk-socialmedia--footer
			 <?php if (count($variables['socialMediaLinks']['footer']) > 3): print "uk-socialmedia--compact"; endif; ?>
			">
			<?php if ($block_title = (theme_get_setting('block_title'))): ?>
				<h2><?php echo $block_title; ?></h2>
			<?php endif; ?>
			<ul class="uk-socialmedia__list">
				<?php foreach($variables['socialMediaLinks']['footer'] as $accountName => $account){ ?>
					<li class="uk-socialmedia__list-item">
						<a class="uk-socialmedia__link"href="<?php echo $account['url']; ?>" target="_blank"><span class="uk-socialmedia__name"><?php print $accountName;?></span><i class="fa <?php echo $account['icon']; ?>" ></i></a>
					</li>
				<?php };?>
			</ul>
		</div>
	<?php }; ?>
<?php endif; ?>

