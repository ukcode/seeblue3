<footer id="footer-wrapper">

    <div class="section">


        <div id="footer" class="wrap-inner  container">

            <div class="footer-top cf row">


                <?php if ($page['footer_first_column']): ?>

                    <?php print render($page['footer_first_column']); ?>

                <?php endif; ?>

                <?php if ($page['footer_second_column']): ?>

                    <?php print render($page['footer_second_column']); ?>

                <?php endif; ?>

                <?php if ($page['footer_third_column']): ?>

                    <?php print render($page['footer_third_column']); ?>

                <?php endif; ?>

            </div>

            <div class="footer-bottom cf row">
			<div class="col-md-8">
                <?php if ($page['footer']): ?>                    
						<?php print render($page['footer']); ?>
		        <?php endif; ?>
			</div>

                <div class="col-md-4">
					<?php include('uk-socialmedia--footer.inc'); ?>
                </div>

            </div>

        </div>

    </div>

</footer>