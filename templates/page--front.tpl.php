<?php 
/**
 * @file
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template normally located in the
 * modules/system directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 * - $hide_site_name: TRUE if the site name has been toggled off on the theme
 *   settings page. If hidden, the "element-invisible" class is added to make
 *   the site name visually hidden, but still accessible.
 * - $hide_site_slogan: TRUE if the site slogan has been toggled off on the
 *   theme settings page. If hidden, the "element-invisible" class is added to
 *   make the site slogan visually hidden, but still accessible.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['header']: Items for the header region.
 * - $page['featured']: Items for the featured region.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['triptych_first']: Items for the first triptych.
 * - $page['triptych_middle']: Items for the middle triptych.
 * - $page['triptych_last']: Items for the last triptych.
 * - $page['footer_firstcolumn']: Items for the first footer column.
 * - $page['footer_secondcolumn']: Items for the second footer column.
 * - $page['footer_thirdcolumn']: Items for the third footer column.
 * - $page['footer_fourthcolumn']: Items for the fourth footer column.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see bartik_process_page()
 * @see html.tpl.php
 */


$is_horizontal = theme_get_setting('use_horizontal_menu');
$menu = menu_navigation_links('main-menu');


?>

<?php include_once('includes/uk-global-header.inc'); ?>

	<!-- Start Sitewide Alert -->
	  <?php if ($alert_message): ?>
		<div id="alert-bg"><div id ="sitewide_alert">
			<?php if ($alert_title): ?>
				<?php print '<h2>' . $alert_title . '</h2>'; ?>
				<hr class="alert_separator" />
				<br>
			<?php endif; ?>
			<?php print $alert_message; ?>
		</div></div>
	  <?php endif; ?>
	  <!-- End Sitewide Alert -->

<div id="page-wrapper"  class="middle-container">

    <?php include_once('includes/uk-offcanvas.inc'); ?>
  
  <div id="page">
	
    <!-- start header -->
    <?php include('includes/header.inc'); ?>
    <?php // print render($page['header']); ?>
    <!-- end header --> 


	<div id="main-wrapper" class="container uky-offcanvas__content">
    <?php if ($is_horizontal == 1): ?>
        <?php include('includes/primary-nav--horizontal.inc') ?>
    <?php endif; ?>
	
	<?php if ($page['content_header']): ?>
		<div class="col-md-12 vspace">
			<?php print render($page['content_header']); ?>	
		</div>	
    <?php endif; ?>
	
	<?php if ($page['showcase']): ?>
		<div class="col-md-12 vspace">
			<?php print render($page['showcase']); ?>
		</div>
    <?php endif; ?>
	
	
	
    
        <div id="main" class="wrap-inner cf row">

            <section id="content" class="col-md-9 col-md-push-3 col-sm-12">
                <div class="content-list cf">
					<?php if ($promoted_to_front == 1): ?>
						<div class="block site-messages">
							<?php print $messages;?>
						</div>
						<?php print render($tabs); ?>
						<?php print render($title_prefix); ?>
						<?php if ($title): ?>
							<h1 class="title page-header" id="page-title">
								<?php print $title; ?>
							</h1>
						<?php endif; ?>
						<?php print render($title_suffix); ?>
						<?php print render($page['homepage_content']); ?>
						<h1 class="promoted_to_front"><?php print $promoted_to_front_title; ?></h1>
						<?php print render($page['content']); ?>
						
						
							
					<?php else: ?>
						
						<div class="block site-messages">
							<?php print $messages;?>
						</div>
						<?php print render($tabs); ?>
						<?php print render($page['homepage_content']); ?>
					<?php endif; ?>	

                </div>

            </section>


            <div class="sidebar col-md-3 col-md-pull-9 col-sm-12">
                <?php include('includes/primary-nav--vertical.inc'); ?>
				<?php include('includes/uk-socialmedia--sidebar.inc'); ?>
				

				<!-- start first sidebar -->
                <?php if ($page['sidebar_first']): ?>
                    <aside id="sidebar-first" class="sidebar">
						<?php print render($page['sidebar_first']); ?>
						<?php $is_sidebar = TRUE; ?>
					</aside>
				<?php endif; ?>
				<!-- end first sidebar -->
			</div>
       </div>
    </div>
	
<!-- end content -->


        <?php include('includes/seeblue-footer.inc'); ?>
        <?php include('includes/uk-global-footer.inc'); ?>

  </div>

</div>
