<?php

/**
 * @file
 * Default theme implementation for displaying search results.
 *
 * This template collects each invocation of theme_search_result(). This and
 * the child template are dependent to one another sharing the markup for
 * definition lists.
 *
 * Note that modules may implement their own search type and theme function
 * completely bypassing this template.
 *
 * Available variables:
 * - $search_results: All results as it is rendered through
 *   search-result.tpl.php
 * - $module: The machine-readable name of the module (tab) being searched, such
 *   as "node" or "user".
 *
 *
 * @see template_preprocess_search_results()
 *
 * @ingroup themeable
 */
?>
<?php $site_name =  variable_get('site_name', 'Drupal'); ?>
<?php $searchString = get_current_search_terms(); ?>
<?php if ($search_results): ?>

    <h2><?php print t('Results for "'.$searchString.'" on '. $site_name );?></h2>
    <ol class="uk-searchresults <?php print $module; ?>-results">
        <?php print $search_results; ?>
    </ol>
    <?php print $pager; ?>
<?php else : ?>
    <h2><?php print t('No results for "'.$searchString.'" on '. $site_name );?></h2>
    <?php //print search_help('search#noresults', drupal_help_arg()); ?>
<?php endif; ?>

<div  class="uk-searchresults--google">
    <h2>Results for "<?php print $searchString; ?>" elsewhere on uky.edu</h2>
    <div class="uk-searchresults__google-branding">
        powered by <img src="https://www.google.com/uds/css/small-logo.png" alt="google"> Custom Search Engine
    </div>
    <div id="GCSEresults"></div>

    <a class="uk-searchresults__more" href="https://cse.google.com/cse?cx=006049166686491933951:ssb7y1fquzc&ie=UTF-8&resultsUrl=http://ukcc.uky.edu/cse/&queryParameterName=q&q=<?php print rawurlencode($searchString);?>#gsc.tab=0&gsc.q=test&gsc.page=1">More "<?php print $searchString;?>" results from all of uky.edu</a>
</div>


<script>
    function gcseCallback() {
        if (document.readyState != 'complete')
            return google.setOnLoadCallback(gcseCallback, true);
        google.search.cse.element.render({gname:'gsearch', div:'GCSEresults', tag:'searchresults-only', attributes:{linkTarget:'', enableImageSearch:false}});
        var element = google.search.cse.element.getElement('gsearch');
        element.execute("<?php print $searchString;?>");
    };
    window.__gcse = {
        parsetags: 'explicit',
        callback: gcseCallback
    };
    (function() {
        var cx = '006049166686491933951:ssb7y1fquzc';
        var gcse = document.createElement('script');
        gcse.type = 'text/javascript';
        gcse.async = true;
        gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
            '//www.google.com/cse/cse.js?cx=' + cx;
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(gcse, s);
    })();
</script>

