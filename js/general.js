   function position_pager()
    {
        if ($(".flexslider").length > 0) {

            //get the height of the images in the slider
            var img_height = $(".flexslider .slides li:first-child img").css("height");

            //convert "px" string to integer
            img_height = img_height.substr(0, img_height.length-2) * 1;

            //get the height of the pager
            var pager_height = $(".flexslider .flex-control-nav").css("height");

            //convert "px" string to integer
            pager_height = pager_height.substr(0, pager_height.length-2) * 1;

            //calculate the final position of the pager (includes 10 px of padding)
            var pager_top = (img_height - 40) + 'px';

            $(".flexslider .flex-control-nav").css("top", pager_top);
            $(".flexslider .flex-control-nav").css("display", "block");
        }
        
    }

// Utilizing the Modernizr object created to implement placeholder functionality
$(document).ready(function(){

    /*
    // Add novalidate tag if HTML5.
    if (!$.browser.msie || $.browser.version > 7) {
        this.attr('novalidate', 'novalidate');
    }
    */

    $(".mobile-menu ul.menu").css("display", "none");


        /*
        !!!DEPRECATED!!!
     Only initialize the slider plugin if we have included the script (script is automatically included when slider markup is detected - see template.php)
     */
    if (typeof $(".rslides").responsiveSlides == 'function')
    {
        //use this variable to toggle the pager for the slideshow - if we have a pager add the margin-bottom to the pager instead of the slideshow itself
        var hasPager = true;

        $(".rslides").responsiveSlides({
            pager: hasPager,
            nav: true,
            prevText: "",
            nextText: "",
            timeout: 10000
        });

        setTimeout(position_pager, 1000);

        //(hasPager == false) ? $(".rslides").css("margin-bottom", "2em") : $(".rslides_tabs").css("margin-bottom", "2em");
    }



    //  if the markup and scripts for the slider are in place, initialize it
    if (typeof $(".flexslider").flexslider == 'function') {
        $('.flexslider').flexslider({
            prevText: "",
            nextText: ""
        });


        setTimeout(position_pager, 1000);
    }

	if(typeof(Modernizr) != 'undefined' && !Modernizr.input.placeholder){
		$('input[type="text"]').each(function(){
			var phAttr = $(this).attr('placeholder');
			if(typeof(phAttr) != 'undefined' && phAttr != false){
				if(phAttr != null && phAttr != ''){
				  $(this).addClass('default_title_text');
				  $(this).val(phAttr);
				  $(this).focus(function(){
					$(this).removeClass('default_title_text');
					if($(this).val() == $(this).attr('placeholder')){
					  $(this).val('');
					}
				  });
				  $(this).blur(function(){
					if($(this).val() == ''){
					  $(this).val($(this).attr('placeholder'));
					  $(this).addClass('default_title_text');}
				  });
				}
			}    
		});
	}


	// toggle the display of the menu when in reponsive mobile layout
	$('.menulink').click(function(){
		//$('.block-menu .menu:first-child').slideToggle("fast");
//		$('#block-system-main-menu .menu:first-child').slideToggle("fast");
		$(".mobile-menu .menu:first-child").slideToggle('fast');
		//$('#block-system-main-menu .secondary-menu').slideToggle("fast");
		
	});

	//add class to menu list elements (li) based on whether they are parents or leaves
//	$('.block-menu .menu li').each(function(index) {

	$('.menu li').each(function(index) {
		if ($(this).children().length < 2) {
			$(this).addClass('plainlink');	//add 'plainlink' class to leaves
		}
		else {

			//$(this).addClass('parentlink');	//add 'parentlink' class to parents
            //$(this).children('a').attr('href', 'javascript:void(0);');
            //
			//if($(this).find('a').hasClass('active')){
			//	$(this).addClass('open');
			//}
		}
	});

	//$('.primary-nav--vertical .parentlink a').click(function(e){
	//	if($(this).parent().hasClass('open')){
	//		$(this).parent().removeClass('open').addClass('closed');
	//	} else {
	//		$(this).parent().removeClass('closed').addClass('open');
	//	}
	//});


    $(".sidebar #block-system-main-menu ul.menu a.active:first").siblings("ul.menu").each(function (index) {
        //$(this).css("display", "block");
        return false;
    });

	//$('.menu li.parentlink > a').attr('href', 'javascript:void(0);');

	//toggle the display of child links when a parent is clicked
	$(".parentlink > a").click(function(e) {
		if ($(window).width() <= 640)
		{
			$(this).siblings("ul").slideToggle('slow');
		}
		//e.preventDefault();
	});


	//toggle the display of child links when a parent is clicked
	$(".sidebar .block-menu .parentlink > a").click(function() {
		if ($(window).width() > 640)
		{
			$(this).siblings("ul").slideToggle('slow');
		}
	});
	
	$("#content-header .block-menu > .content > .menu > li").mouseover(function() {
		if ($(window).width() > 640)
		{
			$(this).addClass("active");
		}
	});
	$("#content-header .block-menu > .content > .menu > li").mouseout(function() {
		if ($(this).hasClass("active"))
		{
			$(this).removeClass("active");
		}
	});
	
	
	//when a child link is hovered over, 
	$("#content-header .block-menu > .content > .menu > li li").mouseover(function() {
		$(this).parents("li.parentlink").children("a").addClass("highlight");
	});
	$("#content-header .block-menu > .content > .menu > li li").mouseout(function() {
		$(this).parents("li.parentlink").children("a").removeClass("highlight");
	});



	/*
		Check the window size when it's re-sized to see if we need to hide/show the menu
	*/
	$(window).resize(function() {
		var windowWidth = $(window).width();
		if (windowWidth > 991){
			$('.uky-offcanvas').data('ukyOffCanvas').collapse();
		}
        position_pager();

	});



	//$("#tabs").tabs();


    /*
    When the main nav menu is in the sidebar, expand the nested menus as appropriate to show the current page's link
     */
    //if ($(window).width() > 640 ) {
    //    //$(".sidebar  .menu li a.active").parents("ul").css("display", "block");
    //}
    //



    $("#mobile-search-toggle").click(function() {
        if ($('.searchform input[name="q"]').val().length > 0 && $(".searchform").css("display") == 'block') {
            $("form.searchform").submit();
        }
        else {
            $(".wrap-inner .searchform").slideToggle();
        }
    });

});

   //verticalMenu
   function verticalMenu($el){
	   var that = this;
	   this.$root = $el;
	   this.$list = this.$root.children('ul');
	   this.$root.data('verticalMenu', self);
	   this.submenus = [];
	   this.$list.children('li').each(function(i, el){
		   if ($(el).children().length > 1) {
			   that.submenus.push(
				   new expandableMenu($(el), that,  that.submenus.length)
			   )
		   }
	   })

	   this.collapse = function(except){
		   $.each(that.submenus, function(i, submenu){
			   if (i != except){
			   	submenu.collapse();
			   }
		   })
	   }
   }



   function expandableMenu($el, parentMenu, i){
	   var that = this;
	   this.$parentMenu = parentMenu;
	   this.index = i;
	   this.$parentListItem = $el;
	   this.$parentListItem.addClass("uky-expandablemenu");
	   this.$expandButton = $('<div class="uky-expandablemenu__toggle"></div>');
	   this.$parentListItem.append(this.$expandButton);
	   this.$parentLink = $(this.$parentListItem.children('a'));
	   this.$menu = this.$parentListItem.children('.menu');

	   //disable other submenu parent links
		this.$menu.find('.menu').each(function(i, el){
			//$(el).siblings('a').attr('href', 'javascript:void(0);');
		})

	   //set this menu to lock open if it is in the active trail
	   if(this.$parentListItem.find('a.active').length > 0){
		   this.$parentListItem.addClass("uky-expandablemenu--active");
	   }

	   this.$expandButton.click(function(){
		   if (!that.$parentListItem.hasClass('uky-expandablemenu--active')){
			   if (that.$parentListItem.hasClass('uky-expandablemenu--open')){
				   that.$parentMenu.collapse();
			   } else {
				   that.$parentMenu.collapse(that.index);
				   that.expand();
			   }
		   }
	   });

	   this.collapse = function(){
		   that.$parentListItem.removeClass('uky-expandablemenu--open');
	   };

	   this.expand = function(){
		   that.$parentListItem.addClass('uky-expandablemenu--open');
	   }
   }

   $(document).ready(function(){
	   jQuery('.uky-verticalmenu').each(function(el){
		   new verticalMenu($(this));
	   });


   });







   //Offcanvas


   function ukyOffCanvas($el, opts) {
	   var opts = opts || {};
	   var self=this;
	   this.$root = $el;
	   this.$root.data('ukyOffCanvas', self);
	   this.$content = jQuery(this.$root.next().find('.uky-offcanvas__content'));
	   this.$button = jQuery('.uky-offcanvas__button');
	   this.onExpand = opts.onExpand;
	   this.onCollapse = opts.onCollapse;

	   this.$button.on('click', function(ev){
		   if (self.$root.attr('aria-hidden') == 'true'){
			   self.expand();
		   } else {
			   self.collapse();
		   }
	   });

	   this.expand = function(){


		   self.onExpand();

		   self.$root.addClass('uky-offcanvas--open');
		   self.$content.addClass('uky-offcanvas__content--open');
		   self.$button.attr('aria-expanded', true);
		   self.$root.attr('aria-hidden', false);
	   };

	   this.collapse = function(){

		   self.onCollapse();

		   self.$root.removeClass('uky-offcanvas--open');
		   self.$content.removeClass('uky-offcanvas__content--open');
		   self.$button.attr('aria-expanded', false);
		   self.$root.attr('aria-hidden', true);
	   }
   }

   $(document).ready(function(){
	   jQuery('.uky-offcanvas').each(function(){
		   new ukyOffCanvas(
			   jQuery(this),
			   {
				   onExpand: function () {
					   $('body').addClass('uk-global-header--fixed');
					   //jQuery('.uky-searchbar').data('searchBar').collapse();
					   //jQuery('.uky-header').data('header').locked = true;
				   },
				   onCollapse: function(){
					   $('body').removeClass('uk-global-header--fixed');
					   //jQuery('.uky-header').data('header').locked = false;
				   }
			   }
		   );
	   })
   });







