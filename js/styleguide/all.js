/**
 * A single file to load all JS for the style guide.
 * This is a placeholder until a proper JS compile task
 * can be put together using browserify
 */



/**
* Accordion
 * based on https://github.com/18F/web-design-standards/blob/18f-pages-staging/src/js/components/accordion.js
 * NOTE: released as CC0 1.0 public domain from the above source
*/

function Accordion($el) {
    var self = this;
    this.$root = $el;
    this.$root.on('click', 'button', function(ev) {
        var expanded = JSON.parse($(this).attr('aria-expanded'));
        ev.preventDefault();
        self.hideAll();
        if (!expanded) {
            self.show($(this));
        }
    });
}

Accordion.prototype.$ = function(selector) {
    return this.$root.find(selector);
}

Accordion.prototype.hide = function($button) {
    var selector = $button.attr('aria-controls'),
        $content = this.$('#' + selector);

    $button.attr('aria-expanded', false);
    $content.attr('aria-hidden', true);
};

Accordion.prototype.show = function($button) {
    var selector = $button.attr('aria-controls'),
        $content = this.$('#' + selector);

    $button.attr('aria-expanded', true);
    $content.attr('aria-hidden', false);
};

Accordion.prototype.hideAll = function() {
    var self = this;
    this.$('button').each(function() {
        self.hide($(this));
    });
};

$('[class^=uk-accordion]').each(function() {
    console.log('new accordiion');
    new Accordion($(this));
});


/**
 * Search bar
 */

function Searchbar($el) {
    var self=this;
    this.$root = $el;
    this.$root.on('click', 'button', function(ev){
        console.log('dismiss');
        self.$root.attr('aria-hidden', true);
    })
}

//TODO: resolve modified versions
$('.uk-search-bar').each(function(){
    new Searchbar($(this));
})


/**
 * * Search button
 */

function SearchButton($el) {
    var self=this;
    this.$root = $el;
    this.$root.on('click', 'button', function(ev){
        //get the target ID
        var targetId = $(this).attr('aria-controls');
        $("#"+targetId).attr("aria-hidden", false).find('input[type="search"]').focus();
    })
}

$('.uk-global-header__search-button').each(function(){
    new SearchButton($(this));
})

