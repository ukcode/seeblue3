<?php

/*
* Initialize theme settings
*/

// print_r(debug_backtrace());
// error_reporting(E_ALL);
// ini_set('display_errors', 1);

// if ( is_null( theme_get_setting( 'site_description' ) ) )
// {
//   global $theme_key;
//   /*
//   * The default values for the theme variables. Make sure $defaults exactly
//   * matches the $defaults in the theme-settings.php file.
//   */
//   $defaults = array(             // <-- change this array
//     'background_logo_path'	=> drupal_get_path('theme',$GLOBALS['theme']) . theme_get_setting('background_logo_path'),
//     'site_description' => " ",
//     'front_logo' => seeblueV3_save_file( theme_get_setting("front_logo")),
//     'interior_logo' => seeblueV3_save_file(theme_get_setting("interior_logo"))
//   );


//   // Get default theme settings.
//   $settings = variable_get(str_replace('/', '_', 'theme_'. $theme_key .'_settings'), array());

//   // Don't save the toggle_node_info_ variables.
//   if (module_exists('node'))
//   {
//     // NOTE: node_get_types() is renamed to node_type_get_types() in Drupal 7
//     foreach (node_type_get_types() as $type => $name)
//     {
//       unset($settings['toggle_node_info_' . $type]);
//     }
//   }

//   // Save default theme settings.
//   variable_set(
//     str_replace('/', '_', 'theme_'. $theme_key .'_settings'),
//     array_merge($defaults, $settings)
//   );

//   // Force refresh of Drupal internals.
//   //theme_get_setting('', TRUE);

//   //Reset the theme_get_setting static cache, so initialized defaults are immediately rendered.
//   drupal_static_reset('theme_get_setting');


// }



function seeblueV3_js_alter(&$js)
{
if (!module_exists('jquery_update')){
$jqKey = "my-new-jquery"; // a key for new jquery file entry
    $js[$jqKey] = $js['misc/jquery.js']; // copy the default jquery settings, so you don't have to re-type them.
    $js[$jqKey]['data'] = "https://code.jquery.com/jquery-1.12.2.min.js"; // the path for new jquery file.
    $js[$jqKey]['version'] = '1.12.2'; // your jquery version.

    unset($js['misc/jquery.js']); // delete drupal's default jquery file.
}

}

function seeblueV3_html_head_alter($head_elements) {
  // Search the head elements for the Favicon
  foreach ($head_elements as $key => $element) {
    if (!empty($element['#attributes'])) {
      if (array_key_exists('href', $element['#attributes'])) {
        if (strpos($element['#attributes']['href'], 'favicon.ico') > 0)
        {
          // Change the URL
          $head_elements[$key]['#attributes']['href'] = '/'.drupal_get_path('theme', 'seeblueV3').'/img/styleguide/favicon-96x96.png';

          // Or, delete the favicon link entirely
         // unset($head_elements[$key]);
        }
      }
    }
  }
}

/**
 *
 *	Preprocess each image used in the template.
 *
 *	@param &$variables - object containing the data used to build the markup for each image inserted into the document
 *
 *	@author Miles Briggs
 *
 */
function seeblueV3_preprocess_image(&$variables)
{
  $directories = array('slideshow', 'teaser_thumb', 'page_header');

  foreach ($directories as $d)
  {
    // if the image has the any image style defined in $directories applied to it, remove the height and width attributes to make it responsive
    if (strpos($variables['path'], $d) > 0)
    {
      unset($variables['height']);
      unset($variables['width']);
    }
  }
}

/**
 *
 *	Preprocess function for views.
 *
 *	@param &$variables - object containing the data used to build the markup for a view
 *
 *	@author Miles Briggs
 */
function seeblueV3_preprocess_views_view(&$variables)
{
  // if we have a slideshow view, add the appropriate scripts and stylesheets
  if ($variables['view']->human_name == "slideshow_output")
  {
    drupal_add_css(path_to_theme() . '/css/flexslider.css');
//    drupal_add_js('sites/all/themes/seeblueV3/js/responsiveslides.min.js');
  }
}

/**
 *
 *	Preprocess function for page.
 *
 *	@param &$variables - object containing the data used to build the markup for the page content
 *
 *	@author Miles Briggs
 *
 */
function seeblueV3_preprocess_page(&$variables)
{
  /**
   *	By default, drupal's main menu will only render as a single level, i.e., all child links are ignored.
   *	Because of that, we have to explicitly tell drupal we want the entire depth of the menu.
   */
  $main_menu_tree = menu_tree_all_data('main-menu');						//get the raw values for the entire menu object
  $variables['main_menu_expanded'] = menu_tree_output($main_menu_tree);	//build a renderable array out of the menu data and store as a variable we can use in our page template
//	print_r($variables);
//	$variables['page']['content_header']['system_main-menu'] = $variables['page']['sidebar_first']['system_main-menu'];

/**
Search block
*/
    $block = module_invoke('search','block_view','search');
    $rendered_block = render($block);
    $variables['mysearchblock'] = $rendered_block;


/* social media */
  $social_location = theme_get_setting('social_location');
  $block_title = theme_get_setting('block_title');


  $socialAccounts = array(
      'facebook'=>'fa-facebook-official',
      'google'=>'fa-google-plus-square',
      'twitter'=>'fa-twitter-square',
      'instagram'=>'fa-instagram',
      'youtube'=>'fa-youtube-square'
  );

  $socialMediaLinks = array();


  foreach ($socialAccounts as $accountName=>$icon){
    if ($accountUrl = theme_get_setting($accountName)){
      if ($social_location == 1 || $social_location == 3){
        $socialMediaLinks['sidebar'][$accountName] = array('url'=>$accountUrl, 'icon'=>$icon);
      }
      if ($social_location == 2 || $social_location == 3){
        $socialMediaLinks['footer'][$accountName] = array('url'=>$accountUrl, 'icon'=>$icon);
      }
    }
  }

	$variables['socialMediaLinks'] = $socialMediaLinks;
	
	if ($socialMediaLinks = 0) {
		$variables['social_location'] = 4;
	} else {
		$variables['social_location'] = $social_location;
	}
	
	  
  
  
  /* Setting other variables for use in template & include files */
	$variables['promoted_to_front'] = theme_get_setting('promoted_to_front');
	$variables['promoted_to_front_title'] = theme_get_setting('promoted_to_front_title');
	$variables['show_breadcrumb'] = theme_get_setting('show_breadcrumb');
	$variables['default_banner'] = theme_get_setting('default_banner');
	$variables['login_links'] = theme_get_setting('login_links');
	$variables['alert_message'] = theme_get_setting('alert_message');
	$variables['alert_title'] = theme_get_setting('alert_title');
	$variables['menu_selection'] = theme_get_setting('menu_selection');
	$variables['sidebar_submenu'] = theme_get_setting('sidebar_submenu');
	$variables['boxed_body'] = theme_get_setting('boxed_body');
	$variables['boxed_color'] = theme_get_setting('boxed_color');
	$variables['boxed_tile'] = theme_get_setting('boxed_tile');
	
	$variables['base'] = $GLOBALS['base_path'];
}



/**
 *	Theme hook for drupal forms.
 *
 *	@author Miles Briggs
 *
 */
function seeblueV3_form_alter(&$form, &$form_state, $form_id)
{
  if ($form_id == 'search_block_form')		//hook the search form to modify some attributes
  {
    //add a placeholder for the search form text field (not included by default)
	$form['search_block_form']['#attributes']['placeholder'] = theme_get_setting('search_placeholder');//"Search this site";
	
	//change the submit field to use an image rather than a button
	$form['actions']['submit'] = array('#type' => 'image_button', '#src' => base_path() . path_to_theme() . '/img/search-iconbg.png');
  }

}


/**
*Private theme function to create a file record
*/
function seeblueV3_save_file($uri)
{
global $user;
$uri = drupal_get_path('theme',$GLOBALS['theme']) . $uri;
$filename = end((explode('/', $uri)));

$file = new stdClass;
$file->uid = $user->uid;
$file->filename = $filename;
$file->uri = $uri;
$file->filemime = file_get_mimetype($uri);
$file->filesize = filesize($uri);
$file->status = 1;
file_save($file);

file_usage_add($file, $GLOBALS['theme'], 'user', '1');

return $file->fid;
}




function  seeblueV3_menu_topSectionTree(){
$menu_selection = theme_get_setting('menu_selection');
  $tree = menu_tree_all_data($menu_selection);
  menu_tree_add_active_path($tree);
  //print_r($tree);

  //find the active section
  foreach ($tree as $key => $element) {
    if ($element['link']['in_active_trail'] == 1 && isset($element['below']) && $element['link']['link_path']!='<front>'){
      return array($key => $element);
    }
  };
  return NULL;
}

//
//function seeblueV3_menu_get_subtree($tree, $mlid) {
//
//  //get the current section mlid (top level section)
//
//  foreach ($tree as $key => $element) {
//    // Is this the entry we are looking for?
//    if ($mlid == $element['link']['mlid'])  {
//      // Yes, return while keeping the key
//      return array($key => $element);
//    }
//    else {
//      // No, recurse to children, if any
//      if ($element['below']) {
//        $submatch = seeblueV3_menu_get_subtree($element['below'], $mlid);
//        // Found wanted entry within the children?
//        if ($submatch) {
//          // Yes, return it and stop looking any further
//          return $submatch;
//        }
//      }
//    }
//  }
//  // No match at all
//  return NULL;
//}

function menu_tree_add_active_path(&$tree) {
  // Grab any menu item to find the menu_name for this tree.
  $menu_item = current($tree);
  $tree_with_trail = menu_tree_page_data($menu_item['link']['menu_name']);

  // To traverse the original tree down the active trail, we use a pointer.
  $subtree_pointer = &$tree;

  // Find each key in the active trail.
  while ($tree_with_trail) {
    foreach ($tree_with_trail as $key => &$value) {
      if ($tree_with_trail[$key]['link']['in_active_trail']) {
        // Set the active trail info in the original tree.
        $subtree_pointer[$key]['link']['in_active_trail'] = TRUE;
        // Continue in the subtree, if it exists.
        $tree_with_trail = &$tree_with_trail[$key]['below'];
        $subtree_pointer = &$subtree_pointer[$key]['below'];
        break;
      }
      else {
        unset($tree_with_trail[$key]);
      }
    }
  }
}
//
//function seebluev3_preprocess_search_results(&$variables) {
//  if (isset($variables['search_results'])){
//    $variables['theme_hook_suggestions'] = 'search_results';
//  }
//}

//function seebluev3_preprocess_search_result(&$variables) {
//  if (isset($variables['search_result'])){
//    $variables['theme_hook_suggestions'] = 'search_result';
//  }
//}

function get_current_search_terms() {
// only do this once per request
  static $return;
  if (!isset($return)) {
    // extract keys from path
    $path = explode('/', $_GET['q'], 3);
    // only if the path is search (if you have a different search url, please modify)
    if(count($path) == 3 && $path[0]=="search") {
      $return = $path[2];
    } else {
      $keys = empty($_REQUEST['keys']) ? '' : $_REQUEST['keys'];
      $return = $keys;
    }
  }
  return $return;
}


function seeblueV3_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];
  if (!empty($breadcrumb)) {
    // Adding the title of the current page to the breadcrumb.
    $breadcrumb[] = drupal_get_title();
    
    // Provide a navigational heading to give context for breadcrumb links to
    // screen-reader users. Make the heading invisible with .element-invisible.
    $output = '' . t('') . '';

    $output .= '' . implode(' / ', $breadcrumb) . '';
    return $output;
  }
}







