<?php

/**
 *
 *  @function seeblueV3_form_system_theme_settings_alter - implements a hook for the seeblueV3 theme settings - adds additional fields and rearranges existing fields for consistency
 *
 *  @param &$form - array that contains all the data necessary to generate the theme settings form. Passed by reference.
 *  @param $form_state - array that describes the current state of the form (e.g., any edits, updates, deletions, etc...)
 *
 *  @author Miles Briggs
 *
 */
 
 //check for enabled seeblue module.
  if (!module_exists('sb'))  {
	  $apath = base_path( );
    print "<div style='margin: 100px; height: 100%; text-align: center; line-height: normal;'>"  . t( '<p style="color: #0033a0; font-size: 55px;">seeblue V3 requires the See Blue Custom module to be enabled.</p>  <br><br> <p style="font-size: 30px;">Please enabled the module <a href="' . $apath . 'admin/modules#edit-modules-search">HERE</a> before making any changes to the seeblue V3 theme settings.</p>')
        . "</div>";
  }

/* Build List of menus */
function seeblueV3_get_all_menus() {
      $all_menus = menu_get_menus();
  return $all_menus;
}



function seeblueV3_form_system_theme_settings_alter(&$form, $form_state)
{
	 
 // The "appearance" dialog is not necessarily called from this theme, so path_to_theme does not work.  
 $theme_path = drupal_get_path('theme', variable_get('theme_default', NULL));

 // Get the base-est theme file path rather than the subtheme
 $themes = list_themes();
 $theme_object = $themes[variable_get('theme_default', NULL)];
 if (isset($theme_object->base_themes)){
  $theme_path = drupal_get_path('theme', array_keys($theme_object->base_themes)[0]);
 }

 $file_path = $GLOBALS['base_path'] . $theme_path; 
  
 unset($form['theme_settings']);

 
 $custom_menus = menu_get_menus($all = FALSE);
 
 
 // Create vertical tabs for all seeblueV3 related settings.
  $form['seeblue'] = array(
    '#type' => 'vertical_tabs',
    //'#attached' => array(
    //  'js'  => array(drupal_get_path('libraries', 'bootstrap') . '/js/bootstrap.admin.js'),
    //),
    '#prefix' => '<h2><small>' . t('seeblueV3 Settings') . '</small></h2>',
    '#weight' => -100,
  );

  
  
  // Branding & Identity.
  $form['branding'] = array(
    '#type' => 'fieldset',
    '#title' => t('Branding & Identity'),
	'#description' => t(' '),
	'#weight' => 1,
    '#group' => 'seeblue',
  );
  
  //LOCKUPS

    $lockups_dir = $file_path . '/img/lockups';
	
	/* DIFF */
	$form['branding']['#type'] = 'fieldset';
    $form['branding']['#weight'] = -1;
    $form['branding']['#title']  = t('Branding & Identity');
    $form['branding']['#description'] = t('Add your website name and lockup.');
	/* END DIFF */

    $form['branding']['#description'] = t('Add your website name and lockup.');
    //    $form['branding']['lockup_select'] =  array(
    //        '#type'        => 'select',
    //        '#title'      => t('Primary Lockup'),
    //        '#default_value'  => theme_get_setting('background_logo_select')
    //    );

    $form['branding']['lockup'] = array(
        '#type' 				=> 'managed_file',
        '#title'				=> t('Approved lockup to use in header'),
        '#required'				=> FALSE,
        '#upload_location'		=> file_default_scheme() . '://theme/logos/',
		'#description'			=> t('If your site belongs to a primary unit such as a College, School, or center, upload the approved lockup provided by Public Relations & Marketing. You can obtain these lockups from <a href="http://www.uky.edu/prmarketing/brand-standards" target="_blank">www.uky.edu/prmarketing/brand-standards</a>.  If no lockup is added here, the University lockup will be used.'),
        '#default_value'		=> theme_get_setting('lockup', 'seeblueV3'),
        '#upload_validators'	=> array(
            'file_validate_extensions'		=> array('png svg'),
        ),

    );

    $form['branding']['subsite_title'] = array(
        '#type'        => 'textfield',
        '#title'      => t('Sub-site Title'),
        '#default_value'  => theme_get_setting('subsite_title'),
        '#size'        => 60,
        '#maxlength'    => 128,
        '#required'     => FALSE,
        '#description'    => t('If your site is a sub-site of your primary unit (e.g. a department site within a College), add your site title here.'),
    );
    


    //grab the logo path and upload fields so we can unset some fields and add them back afterwards
    $lp = $form['logo']['settings']['logo_path'];
    $lu = $form['logo']['settings']['logo_upload'];
  
  
  
  // HOMEPAGE.
  $form['logo'] = array(
    '#type' => 'fieldset',
    '#title' => t('Front Page'),
	'#description' => t(' '),
	'#weight' => 2,
    '#group' => 'seeblue',
  );
  
    //unset the conditional fields for providing a default logo - not necessary for this theme
    unset($form['logo']['default_logo']);
    unset($form['logo']['settings']);

    //add the logo upload and path fields back into the updated form object
    $form['logo']['logo_path'] = $lp;
    $form['logo']['logo_path']['#access'] = 0;
    $form['logo']['logo_upload'] = $lu;
    $form['logo']['logo_upload']['#access'] = 0;

      //create an additional field for the site description
      $form['logo']['site_description'] = array(
        '#type'        => 'textarea',
        '#title'      => t('Header text'),
        '#default_value'  => theme_get_setting('site_description'),
        '#description'    => t("This field contains the description text that will be rendered in the header of the site."),
        '#weight'      => -20
      );

	//go ahead and generate the path for the default background logo as specified in seeblueV3.info
	$logo_path = theme_get_setting('background_logo_path');
    
    if (file_uri_scheme($logo_path) == 'public')
    {
    	$logo_path = file_uri_target($logo_path);
    }

	  //Default Banner Image
	  $form['logo']['default_banner'] = array(
      '#type'        => 'select',
      '#title'      => t('Default Banner Image'),
	  '#description' => t('More options will be available '),
      '#options'      => array(
		//$file_path . '/img/banners/seeblue-stacked-dark.png' => t('SeeBlue Stacked - Dark'),
		//$file_path . '/img/banners/seeblue-stacked-light.png' => t('SeeBlue Stacked - Light'),
		//$file_path . '/img/banners/seeblue-full-dark.png' => t('SeeBlue Full - Dark'),
		//$file_path . '/img/banners/seeblue-full-light.png' => t('SeeBlue Full - Light'),
		//$file_path . '/img/banners/seeblue-tile-dark.png' => t('SeeBlue Tile - Dark'),
		//$file_path . '/img/banners/seeblue-tile-light.png' => t('SeeBlue Tile - Light'),
		//$file_path . '/img/banners/uk-tile-dark.png' => t('UK Tile - Dark'),
		//$file_path . '/img/banners/uk-tile-light.png' => t('UK Tile - Light'),
		$file_path . '/img/banners/no-logo-dark.png' => t('No Logo - Dark'),
		//$file_path . '/img/banners/no-logo-light.png' => t('No Logo - Light'),
      ),
      '#default_value'  => theme_get_setting('default_banner', 'seeblueV3')
	);
	
	    //Banner upload
		$form['logo']['homepage_banner'] = array(
        '#type' 				=> 'managed_file',
        '#title' => t('Custom Banner Image'),
		'#description'				=> t('This will override the default banner image.'),
        '#required'				=> FALSE,
        '#upload_location'		=> file_default_scheme() . '://theme/banners/',
		'#description'			=> t('This will render as a background of the front page banner.'),
			'#default_value'		=> theme_get_setting('homepage_banner', 'seeblueV3'),
        '#upload_validators'	=> array(
            'file_validate_extensions'		=> array('png jpg jpeg'),
        ),

    );
	
	// Select option to display content on front page that has Promoted to front page selected.
	  $form['logo']['promoted_to_front'] = array(
		  '#type'        => 'checkbox',
		  '#title'      => t("Display content on front page that has - Promoted to front page - publishing option selected?"),
		  '#default_value'  => '0'
	  );
  
	  $form['logo']['promoted_to_front'] = array(
		  '#type'        => 'select',
		  '#title'      => t('Display content on front page that has - Promoted to front page - publishing option selected?'),
		  '#options'      => array(
			'1' => t('Yes'),
			'0' => t('No'),
		  ),
		  '#default_value'  => theme_get_setting('promoted_to_front')
	  ); 

		$form['logo']['promoted_to_front_title'] = array(
        '#type'        => 'textfield',
        '#title'      => t('Title for - Promoted to front page - content?'),
        '#description' => t('example titles:  Blog, Highlights, Recent Posts'),
		'#default_value'  => theme_get_setting('promoted_to_front_title'),
        '#size'        => 60,
        '#maxlength'    => 128,
        '#required'     => FALSE,
    );


  
  
  
  
  
   // Nav.
  $form['nav'] = array(
    '#type' => 'fieldset',
    '#title' => t('Navigation'),
	'#description' => t(' '),
	'#weight' => 3,
    '#group' => 'seeblue',
  );

$menus = seeblueV3_get_all_menus();
	$form['nav']['menu_selection'] = array(
		'#type'        => 'select',
        '#title'      => t('Select the menu to render in the templates.'),
	    '#description' => t('Default is "Main menu" but you can override that here.'),	
		'#options' => $menus,
		'#default_value'  => theme_get_setting('menu_selection')
	  );
 
  $form['nav']['use_horizontal_menu'] = array(
      '#type'        => 'select',
      '#title'      => t('Use Horizontal Menu?'),
      '#options'      => array(
        '1' => t('Yes'),
        '0' => t('No'),
      ),
      '#default_value'  => theme_get_setting('use_horizontal_menu')
  );
  
  $form['nav']['sidebar_submenu'] = array(
      '#type'        => 'select',
      '#title'      => t('Sidebar Submenu'),
	  '#description'      => t('If using the Horizontal Menu above, do you want the sub menus of the selected menu item displayed in the sidebar?'),
      '#options'      => array(
        '1' => t('Yes'),
        '0' => t('No'),
      ),
      '#default_value'  => theme_get_setting('sidebar_submenu')
  );
  
  $form['nav']['show_breadcrumb'] = array(
      '#type'        => 'select',
      '#title'      => t('Enable breadcrumb on interior pages?'),
      '#options'      => array(
        '1' => t('Yes'),
        '0' => t('No'),
      ),
      '#default_value'  => theme_get_setting('show_breadcrumb')
  );



  
  
  
  
   // Layout & Styling.
  $form['layout_styling'] = array(
    '#type' => 'fieldset',
    '#title' => t('Layout & Styling'),
	'#description' => t(' '),
	'#weight' => 4,
    '#group' => 'seeblue',
  );
  
  // Body
  $form['layout_styling']['body'] = array(
    '#type' => 'fieldset',
    '#title' => t('Body'),
	'#description' => t('* Look for these options in a future release.'),
	);
		/*
		$form['layout_styling']['body']['boxed'] = array(
			'#type' => 'fieldset',
			'#title' => t('Boxed'),
			'#collapsible' => TRUE,
			'#collapsed' => TRUE,
		  );
			// Option for Flat or boxed body
			$form['layout_styling']['body']['boxed']['boxed_body'] = array(
			  '#type'        => 'checkbox',
			  '#title'      => t("Boxed background"),
			  '#description' => t('Check to use a boxed background instead of flat.'),
			  '#default_value'  => theme_get_setting('boxed_body')
			);
			// Color Selection
			$form['layout_styling']['body']['boxed']['boxed_color'] = array(
			  '#type'        => 'select',
			  '#title'      => t("Boxed Background Color"),
			  '#options' => array(
				'0' => t('White'),
				'1' => t(''),
				),
			  '#default_value'  => theme_get_setting('boxed_color')
			);
			// Tile Image Selection
			$form['layout_styling']['body']['boxed']['boxed_tile'] = array(
			  '#type'        => 'select',
			  '#title'      => t("Boxed Tile Image"),
			  '#options' => array(
				'0' => t('None'),
				'1' => t(''),
				),
			  '#default_value'  => theme_get_setting('boxed_tile')
			);
			
		// Option for Flat or boxed body
			$form['layout_styling']['body']['body_full_width'] = array(
			  '#type'        => 'checkbox',
			  '#title'      => t("Full Width Body"),
			  '#description' => t('Check to enable full width body.'),
			  '#default_value'  => theme_get_setting('body_full_width')
			);
		*/
	
  // Sidebar
  $form['layout_styling']['sidebar'] = array(
    '#type' => 'fieldset',
    '#title' => t('Sidebar'),
	'#description' => t(''),
	);
	
	// Option to change sidebar block titles to different styles
	  $form['layout_styling']['sidebar']['sidebar_title_style'] = array(
		  '#type'        => 'checkbox',
		  '#title'      => t("Highlight Sidebar Block Titles"),
		  '#default_value'  => theme_get_setting('sidebar_title_style')
	  );
 
  // Footer
  $form['layout_styling']['footer'] = array(
    '#type' => 'fieldset',
    '#title' => t('Footer'),
	'#description' => t(''),
	);
	
	// Option to add a login / logout into the global footer
	  $form['layout_styling']['footer']['login_links'] = array(
		  '#type'        => 'checkbox',
		  '#title'      => t("Add Login / Logout link to footer"),
		  '#default_value'  => theme_get_setting('login_links')
	  );
  
  // General
  $form['layout_styling']['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General'),
	'#description' => t('* Look for these options in a future release.'),
	);

  
  
  
  
  // Social Media.
  $form['socialmedia'] = array(
    '#type' => 'fieldset',
    '#title' => t('Social Media'),
	'#description' => t("These are the default social media account options.  If you have an account that you would like added you can <a href='mailto:jason.ullstam@uky.edu?cc=andrew.shooner@uky.edu&subject=seeblue theme social media request&body=Please list any social media accounts you would like to have added below...'>Request Additional Social Media Fields</a>."),
	'#weight' => 5,
    '#group' => 'seeblue',
  );
		$form['socialmedia']['social_location'] = array (
			'#type' => 'select',
			'#title' => t('Social Links Location'),
			'#description' => t('Choose which locations you want the social links to appear.'),
			'#default_value' => theme_get_setting('social_location'),
			'#options' => array(
			'1' => t('Sidebar Only'),
			'2' => t('Footer Only'),
			'3' => t('Sidebar & Footer'),
			'4' => t('None'),
			),
		  );
		$form['socialmedia']['block_title'] = array(
			'#type'          => 'textfield',
			'#title'         => t('Footer social media block title'),
			'#description'   => t("Title for Social Media block in footer region.  (e.g. Connect with us!)  If left blank just the icons will be displayed."),
			'#default_value' => theme_get_setting('block_title'),
			);		
		$form['socialmedia']['facebook'] = array(
			'#type'          => 'textfield',
			'#title'         => t('Facebook'),
			'#description'   => t("Enter the URL of your Facebook page."),
			'#default_value' => theme_get_setting('facebook'),
			);
		$form['socialmedia']['google'] = array(
			'#type'          => 'textfield',
			'#title'         => t('Google+'),
			'#description'   => t("Enter the URL of your Google+ page."),
			'#default_value' => theme_get_setting('google'),
			);
		$form['socialmedia']['twitter'] = array(
			'#type'          => 'textfield',
			'#title'         => t('Twitter'),
			'#description'   => t("Enter the URL of your Twitter page."),
			'#default_value' => theme_get_setting('twitter'),
			);
		$form['socialmedia']['instagram'] = array(
			'#type'          => 'textfield',
			'#title'         => t('Instagram'),
			'#description'   => t("Enter the URL of your Instagram page."),
			'#default_value' => theme_get_setting('instagram'),
			);
		$form['socialmedia']['youtube'] = array(
			'#type'          => 'textfield',
			'#title'         => t('Youtube'),
			'#description'   => t("Enter the URL of your Youtube channel."),
			'#default_value' => theme_get_setting('youtube'),
			);  

  
    // General
  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General'),
	'#description' => t(' '),
	'#weight' => 6,
    '#group' => 'seeblue',
  );
  
  //create a text field to store the search form placeholder text
  $form['general']['search_settings']['search_placeholder'] = array(
      '#type'        => 'textfield',
      '#title'      => t('Search form placeholder'),
      '#default_value'  => theme_get_setting('search_placeholder'),
      '#size'        => 60,
      '#maxlength'    => 128,
      '#required'     => FALSE,
      '#description'    => t('Placeholder in text field of search form.'),
  );

 /*
	
	// Sitewide Alert
  $form['alert'] = array(
    '#type' => 'fieldset',
    '#title' => t('Sitewide Alert'),
	'#description' => t('This is to add a sitewide alert.  Would be used only when an important message needs to be displayed across the entire site.'),
	'#weight' => 7,
    '#group' => 'seeblue',
  );

	//Sitewide Alert Title
	$form['alert']['alert_title'] = array(
        '#type'        => 'textfield',
        '#title'      => t('Alert Title'),
        '#default_value'  => theme_get_setting('alert_title'),
        '#description'    => t("Title for the sitewide alert.  *Not required"),
        '#weight'      => 1
      );
	
	// Sitewide Alert Message
      $form['alert']['alert_message'] = array(
        '#type'        => 'textarea',
        '#title'      => t('Alert Message'),
        '#default_value'  => theme_get_setting('alert_message'),
        '#description'    => t("Message for the sitewide alert.  *Required"),
        '#weight'      => 2
      );
	
	
	*/


  //add a submit handler for the theme settings form
  $form['#submit'][] = 'seeblueV3_settings_submit';



}





/**
 *
 *  @function seeblueV3_settings_submit - submit handler for theme settings form
 *
 *  @param $form - object that contains all the data for the theme settings form
 *  @param $form_state - array that describes the current state of the form (e.g., any edits, updates, deletions, etc...). Passed by reference.
 *
 *  @author Miles Briggs
 */
function seeblueV3_settings_submit($form, &$form_state)
{
	
//	$form_state['values']['background_logo_path'] = $form_state['values']['background_logo_select'];


    if ($form_state['values']['lockup'] != 0)
    {
        $f = file_load($form_state['values']['lockup']);
        $f->status = FILE_STATUS_PERMANENT;
        file_save($f);
		    file_usage_add($f, 'user', 'user', 0);
    }

		if ($form_state['values']['homepage_banner'] != 0)
	{
        $f = file_load($form_state['values']['homepage_banner']);
        $f->status = FILE_STATUS_PERMANENT;
        file_save($f);
		    file_usage_add($f, 'user', 'user', 0);
	}

//
//	if ($form_state['values']['front_logo'] != 0)
//	{
//        $f = file_load($form_state['values']['front_logo']);
//        $f->status = FILE_STATUS_PERMANENT;
//        file_save($f);
//		    file_usage_add($f, 'user', 'user', 0);
//	}
//
}



